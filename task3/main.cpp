#include <iostream>
#include <queue>
#include <vector>
#include <chrono>
#include <thread>
#include <mutex>

using namespace std;

std::vector<string> menu {"Pizza", "Soup", "Steak", "Salad", "Rolls"};
std::queue<int> kitchen;
std::queue<int> delivery;

std::mutex buisy;
int orders = 0;

void makeOrders(){
    while (1) {
        int dish = (int)(5. * std::rand() / RAND_MAX);
        kitchen.push(dish);
        std::cout << menu[dish] << " was pushed in order's list." << std::endl;
        std::this_thread::sleep_for(std::chrono::seconds((int)(5. + 5. * std::rand() / RAND_MAX)));
        orders++;
        if(orders == 10)
            break;
    }
}

void Delivery(){
    while (1) {
        if(delivery.size() > 0){
            int dish = delivery.front();
            delivery.pop();
            std::this_thread::sleep_for(std::chrono::seconds(30));
            std::cout << menu[dish] << " was delivered." << std::endl;
            std::cout << "Orders in the delivery: " << delivery.size() << std::endl;
        }
        else if(orders == 10) {
            break;
        }
    }
}

void makeDish(int dish){
    std::this_thread::sleep_for(std::chrono::seconds((int)(5. + 10. * std::rand() / RAND_MAX)));
    delivery.push(dish);
    std::cout << menu[dish] << " ready." << std::endl;
    buisy.unlock();
}

void Kitchen(){
    while (1) {
        if(buisy.try_lock()){
            if(kitchen.size() > 0){
                int dish = kitchen.front();
                kitchen.pop();
                std::thread orders(makeDish, dish);
                orders.detach();
            }
            else if(orders == 10) {
                break;
            }
        }
    }
}

int main()
{
    std::thread task1(makeOrders);
    std::thread task2(Kitchen);
    std::thread task3(Delivery);
    task1.join();
    task2.join();
    task3.join();
    return 0;
}
