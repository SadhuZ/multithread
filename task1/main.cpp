#include <iostream>
#include <vector>
#include <thread>
#include <chrono>
#include <algorithm>

using namespace std;

struct Swimmer{
    string name;
    double speed;
    double resultTime;
};

vector<Swimmer> swimmers;

void swim(int swimmerNumber, int distance){
    if(swimmerNumber < 0 || swimmerNumber >= swimmers.size())
        return;
    int time = 1000 * distance / swimmers[swimmerNumber].speed;
    for(int i = 1; i <= time / 1000; i++){
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        std::cout << swimmers[swimmerNumber].name << " - " <<
                     swimmers[swimmerNumber].speed * i << "m" << std::endl;
    }
    swimmers[swimmerNumber].resultTime = time / 1000.;
}

int main()
{
    for(size_t i = 0; i < 6; i++){
        Swimmer person;
        cout << "Swimmer " << i + 1 << endl;
        cout << "Name: ";
        cin >> person.name;
        cout << "Speed: ";
        cin >> person.speed;
        swimmers.push_back(person);
    }

    vector<std::thread> way;
    for(size_t i = 0; i < swimmers.size(); i++){
        way.push_back(std::thread(swim, i, 100));
    }
    for(size_t i = 0; i < swimmers.size(); i++)
        way.at(i).join();

    std::sort(swimmers.begin(), swimmers.end(), [](Swimmer a, Swimmer b) {
            return a.resultTime < b.resultTime;
        });

    std::cout << "Results: " << std::endl;
    for(auto person : swimmers){
        std::cout << person.name << " - " << person.resultTime << " sec." << std::endl;
    }

    return 0;
}
