#include <iostream>
#include <chrono>
#include <thread>
#include <mutex>

using namespace std;

std::mutex station_mutex;

void train(char name, int timeToArrival){
    while(1){
        if(station_mutex.try_lock()){
            std::cout << "Train " << name << " start." << std::endl;
            station_mutex.unlock();
            break;
        }
    }

    for(int i = 0; i < timeToArrival; i++)
        std::this_thread::sleep_for(std::chrono::seconds(1));
    std::cout << "Train " << name << " is arriving." << std::endl;

    while(1){
        if(station_mutex.try_lock()){
            std::cout << "Train " << name << " arrived." << std::endl;
            std::string command;
            while(command != "depart"){
                std::cout << "Train " << name << " is waiting for depart." << std::endl;
                std::cin >> command;
            }
            std::cout << "Station is empty." << std::endl;
            station_mutex.unlock();
            break;
        }
    }
}

int main()
{
    int time;
    std::cout << "Train A. Enter a time before arrival: ";
    std::cin >> time;
    std::thread train1(train, 'A', time);
    std::cout << "Train B. Enter a time before arrival: ";
    std::cin >> time;
    std::thread train2(train, 'B', time);
    std::cout << "Train C. Enter a time before arrival: ";
    std::cin >> time;
    std::thread train3(train, 'C', time);
    train1.join();
    train2.join();
    train3.join();
    return 0;
}
